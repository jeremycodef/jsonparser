﻿using System;
using System.Collections.Generic;
using System.Reflection;
using ParseLib;
using Newtonsoft.Json;

namespace ParseDriver
{
    class Program
    {
        static void Main(string[] args)
        {
            int result = 3;
            object myObject = (object)result;
            Console.WriteLine(myObject);
            // Type propType = props[z].PropertyType;
            // MethodInfo method = typeof(Program).GetMethod(nameof(Program.TestFunc), BindingFlags.Static | BindingFlags.Public); method = method.MakeGenericMethod(typeof(string));
            // object parsedType = method.Invoke(null, new object[]{});
        }

        public static T TestFunc<T>() where T : new(){
            Console.WriteLine("test");
            return new T();
        }

        public static void TestCharArr(char[] myCharacters){
            myCharacters[2] = 's';
        }

        public static T SetValues<T>(string arg) where T : new(){
            T myThing = new T();
            PropertyInfo[] props = typeof(T).GetProperties();
            foreach(PropertyInfo prop in props){
                Console.WriteLine($"Property Name: {prop.Name} Value: {prop.GetValue(myThing)}");
                if(prop.Name == "MyThing"){
                    prop.SetValue(myThing, arg);
                }
            }

            return myThing;
        }
    }
}
