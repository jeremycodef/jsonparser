using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ParseLib
{
    public class JsonLexer
    {
        private static readonly char[] JSON_SYNTAX_CHARS = new char[]{'{', '}', ',', ':', '[', ']'};
        public static List<Token> Lex(char[] inputString){
            List<Token> tokenList = new List<Token>();
           //Look at substringing before passing the inputstring around
            for(int i = 0; i < inputString.Length; i++){
                Token currentToken;
                currentToken = JsonLexer.LexString(inputString, i);
                if(currentToken != null){
                    tokenList.Add(currentToken);
                    if(currentToken.endPosition != 0){
                        i = currentToken.endPosition;
                    }
                    continue;
                }
                currentToken = JsonLexer.LexNumber(inputString, i);
                if(currentToken != null){
                    tokenList.Add(currentToken);
                    if(currentToken.endPosition != 0){
                        i = currentToken.endPosition;
                    }
                    continue;
                }

                currentToken = JsonLexer.LexBool(inputString, i);
                if(currentToken != null){
                    tokenList.Add(currentToken);
                    if(currentToken.endPosition != 0){
                        i = currentToken.endPosition;
                    }
                    continue;
                }

                currentToken = JsonLexer.LexNull(inputString, i);
                if(currentToken != null){
                    tokenList.Add(currentToken);
                    if(currentToken.endPosition != 0){
                        i = currentToken.endPosition;
                    }
                    continue;
                }

                if(inputString[i] == ' '){
                    continue;
                }else if(JsonLexer.JSON_SYNTAX_CHARS.Contains(inputString[i])){
                    currentToken = new Token(inputString[i], TokenType.SYNTAX, 0);
                }


                if(currentToken != null){
                    tokenList.Add(currentToken);
                    if(currentToken.endPosition != 0){
                        i = currentToken.endPosition;
                    }
                }else{
                    throw new Exception($"Unparsable character at string position {i}");
                }

            }
            return tokenList;

        } 

        private static Token LexString(char[] inputValue, int currentPos){
            StringBuilder sb = new StringBuilder("", inputValue.Length);
            //First character needs to be ' or "
            if(inputValue[currentPos] != '"'){
                return null;
            }

            char searchCharacter = inputValue[currentPos];
            currentPos += 1;

            for(int i = currentPos; i < inputValue.Length; i++){
                if(inputValue[i] == searchCharacter){
                    // sb.Append(inputValue[i]);
                    return new Token(sb.ToString(), TokenType.STRING, i); 
                }else{
                    sb.Append(inputValue[i]);
                }
            }

            throw new ArgumentException($"Expected end-of-string quote");
       }

       private static Token LexNumber(char[] inputValue, int currentPos){
            char[] numberCharacters = new char[]{'0', '1', '2', '3', '4', '5', '6', '7', '8', '9', '-', 'e', '.'};
            bool isFloat = false;
            int position = 0;
            StringBuilder sb = new StringBuilder("", inputValue.Length);

            for(int i = currentPos; i < inputValue.Length; i++){
                if(numberCharacters.Contains(inputValue[i])){
                    if(inputValue[i] == '.'){
                        isFloat = true;
                    }
                    sb.Append(inputValue[i]);
                }else{
                    break;
                }
                position = i;
            }

            if(sb.Length == 0){
                return null;
            }

            if(isFloat){
                return new Token(Convert.ToDouble(sb.ToString()), TokenType.FLOAT, position);
            }

            return new Token(Convert.ToInt32(sb.ToString()), TokenType.NUMBER, position);
       }
       
       private static Token LexBool(char[] inputValue, int currentPos){
            StringBuilder sb = new StringBuilder("", inputValue.Length);
            int trueLength = "true".Length; // TODO: Make a const
            int falseLength = "false".Length; //TODO: Make a const

            char[] trueCheck = new char[trueLength];
            char[] falseCheck = new char[falseLength];

            if(trueLength + currentPos > inputValue.Length){
                return null;
            }
            if(falseLength + currentPos > inputValue.Length){
                return null;
            }

            Array.Copy(inputValue, currentPos, trueCheck, 0, trueLength);
            if(new string(trueCheck) == "true"){
                return new Token(true, TokenType.BOOLEAN, currentPos + trueLength - 1);
            }

            Array.Copy(inputValue, currentPos, falseCheck, 0, falseLength);
            if(new string(falseCheck) == "false"){
                return new Token(false, TokenType.BOOLEAN, currentPos + falseLength - 1);
            }

            return null;
       }

       private static Token LexNull(char[] inputValue, int currentPos){
            StringBuilder sb = new StringBuilder("", inputValue.Length);
            int nullLength = "null".Length; // TODO: Make a const

            char[] nullCheck = new char[nullLength];

            if(nullLength + currentPos > inputValue.Length){
                return null;
            }

            Array.Copy(inputValue, currentPos, nullCheck, 0, nullLength);
            if(new string(nullCheck) == "null"){
                return new Token(null, TokenType.NULL, currentPos + nullLength);
            }

            return null;
       }
    }
}