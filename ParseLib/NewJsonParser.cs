using System;
using System.Collections.Generic;
using System.Reflection;

namespace ParseLib
{
    public class NewJsonParser
    {
        public static T Parse<T>(List<Token> tokenList, int startPos) {
            return (T)NewJsonParser.InternalParse<T>(tokenList, startPos).data;
        }

        public static ParseData InternalParse<T>(List<Token> tokenList, int startPos){
            if(tokenList[startPos].tokenData.ToString() == "["){
                //Parse array
                return (ParseData)NewJsonParser.ParseArray(tokenList, typeof(T), startPos + 1);
            }else if(tokenList[startPos].tokenData.ToString() == "{"){
                // Parse object
                return (ParseData)NewJsonParser.ParseObject(tokenList, typeof(T), startPos + 1);
            }else{
                // Parse base types
                return new ParseData(startPos + 1, (T)tokenList[startPos].tokenData);
            }
            throw new ArgumentException("Bad stuff happened");

        }

        public static ParseData InternalParseArray<T>(List<Token> tokenList, Type arrayType, int startPos) where T: System.Collections.IList, new(){
            T myList = new T();
            for(int i = startPos; i < tokenList.Count; i++){
                if(tokenList[i].tokenData.ToString() == "]"){
                    return new ParseData(i, myList);
                }else{
                    MethodInfo method = typeof(NewJsonParser).GetMethod(nameof(NewJsonParser.InternalParse), BindingFlags.Static | BindingFlags.Public);
                    method = method.MakeGenericMethod(arrayType.GetGenericArguments()[0]);
                    ParseData parsedData = (ParseData)method.Invoke(null, new object[]{tokenList, i});
                    myList.Add(parsedData.data);
                    i += parsedData.tokensRead - i;
                    if(tokenList[i].tokenData.ToString() == "]"){
                        return new ParseData(i, myList);
                    }
                }
            }
            throw new ArgumentException("Expected an array end bracket");
        }

        public static ParseData ParseArray(List<Token> tokenList, Type arrayType, int startPos){
            MethodInfo method = typeof(NewJsonParser).GetMethod(nameof(NewJsonParser.InternalParseArray), BindingFlags.Static | BindingFlags.Public);
            method = method.MakeGenericMethod(arrayType);
            return  (ParseData)method.Invoke(null, new object[]{tokenList, arrayType, startPos});
        }

        public static ParseData ParseObject(List<Token> tokenList, Type objType, int startPos){
            MethodInfo method = typeof(NewJsonParser).GetMethod(nameof(NewJsonParser.InternalParseObject), BindingFlags.Static | BindingFlags.Public);
            method = method.MakeGenericMethod(objType);
            return  (ParseData)method.Invoke(null, new object[]{tokenList, startPos});
        }

        public static ParseData InternalParseObject<T>(List<Token> tokenList, int startPos) where T: class, new(){
            // throw new NotImplementedException();
            T typeInst = new T();
            PropertyInfo[] props = typeof(T).GetProperties();
            for(int i = startPos; i < tokenList.Count; i++){
                Token currToken = tokenList[i];
                if(currToken.tokenData.ToString() == "}"){
                    return new ParseData(i + 1, typeInst);
                }else{
                    if(currToken.tokenType != TokenType.STRING){
                        throw new ArgumentException($"Expected string key got: {currToken.tokenType}");
                    }

                    if(tokenList[i + 1].tokenData.ToString() != ":"){
                        throw new ArgumentException($"Expected colon after key in object got: {tokenList[i + 2].tokenData}");
                    }

                    // Figure out what 'type' is expected for this key
                    PropertyInfo targetProp = null; 
                    for(int z = 0; z < props.Length; z++){
                        if(props[z].Name == tokenList[i].tokenData.ToString()){
                            targetProp = props[z];
                        }
                    }
                    Type propType = targetProp.PropertyType;
                    if(propType == typeof(string)){
                        targetProp.SetValue(typeInst, tokenList[i+2].tokenData.ToString());
                    }else if(propType == typeof(int)){
                        targetProp.SetValue(typeInst, (int)tokenList[i+2].tokenData);
                    }
                    else if(propType == typeof(bool)){
                        targetProp.SetValue(typeInst, (bool)tokenList[i+2].tokenData);
                    }else{
                        MethodInfo method = typeof(NewJsonParser).GetMethod(nameof(NewJsonParser.InternalParse), BindingFlags.Static | BindingFlags.Public);
                        method = method.MakeGenericMethod(propType);
                        // parsedType = method.Invoke(null, new object[]{tokenList, i + 3});
                        ParseData parseData = (ParseData)method.Invoke(null, new object[]{tokenList, i + 2});
                        targetProp.SetValue(typeInst, parseData.data);
                        // i = parseData.tokensRead + 1;
                        i += parseData.tokensRead - i;
                        if(tokenList[i].tokenData.ToString() == "}"){
                            // i += 1;
                            if(tokenList[i + 1].tokenData.ToString() == ","){
                                i += 1;
                            }
                        }
                        continue;
                    }

                    if(tokenList[i + 3].tokenData.ToString() == "}"){
                        if(tokenList.Count > i + 4 && tokenList[i + 4].tokenData.ToString() == ","){
                            i += 1;
                        }
                        return new ParseData(i + 3, typeInst);
                    }else if(tokenList[i + 3].tokenData.ToString() != ","){
                        throw new ArgumentException($"Expected comma after pair in object got: {tokenList[i+3].tokenData.ToString()}");
                    }
                    i += 3;
                }

            }

            throw new ArgumentException("Expected an end object brace");
        }
    }
}