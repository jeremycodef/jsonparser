// using System;
// using System.Collections.Generic;
// using System.Linq;
// using System.Reflection;

// namespace ParseLib
// {
//     public class JsonParser
//     {
//         public static T Parse<T>(List<Token> tokenList, int startPos) where T : class, new(){
//             if(tokenList[startPos].tokenData.ToString() == "["){
//                 // throw new NotImplementedException();
//                 // return (T)ParseArray<List<Int32>>(tokenList, startPos).data;
//                 throw new NotImplementedException();
//                 // return (T)ParseArray<List<int>>(tokenList, startPos).data[0];
//             }
//             else if(tokenList[startPos].tokenData.ToString() == "{"){
//                 return ParseObject<T>(tokenList, startPos).data;
//             }else{
//                 // This means there should only be one token and the data is the return value,
//                 // But how do we set it to be type T
//                 // which in this case would be int, or string, or bool
//                 T retVal = new T();
//                 retVal = (T)tokenList[startPos].tokenData;
//                 return retVal;
//             }
//         }

//         public static ParseData<T> ParseRecurse<T>(List<Token> tokenList, int startPos) where T : class, new(){
//             if(tokenList[startPos].tokenData.ToString() == "["){
//                 // ParseArray(tokenList);
//                 throw new NotImplementedException();
//             }
//             else if(tokenList[startPos].tokenData.ToString() == "{"){
//                 return ParseObject<T>(tokenList, startPos);
//             }else{
//                 // This means there should only be one token and the data is the return value,
//                 // But how do we set it to be type T
//                 // which in this case would be int, or string, or bool
//                 T retVal = new T();
//                 retVal = (T)tokenList[startPos].tokenData;
//                 return new ParseData<T>(startPos + 1, retVal);
//                 // return ParseBaseType<T>(tokenList, startPos);
//             }
//         }

//         public static ParseData<T> ParseBaseType<T>(List<Token> tokenList, int startPos) where T: class{
//             T myThing;
//             myThing = (T)tokenList[startPos].tokenData;
//             return new ParseData<T>(1, myThing);
//         }

//         public static ParseData<T> ParseArray<T>(List<Token> tokenList, int startPos) where T: class, System.Collections.IList, new(){
//             // T == List<int32>
//             T myList = new T();
//             // if(typeof(T).GetGenericArguments()[0].FullName == "System.Int32"){
//             //     List
//             // }

//             for(int i = startPos; i < tokenList.Count; i++){
//                 Token currToken = tokenList[i + 1];
//                 if(currToken.tokenData.ToString() == "]"){
//                     return new ParseData<T>(startPos - i, myList);
//                 }else if(tokenList[i + 2].tokenData.ToString() == "]"){
//                     myList.Add(currToken.tokenData);
//                     return new ParseData<T>(startPos - i, myList);
//                 }
//                 else if(tokenList[i + 2].tokenData.ToString() != ","){
//                     throw new ArgumentException($"Expected comma after object in array got {tokenList[i + 2].tokenData}");
//                 }
//                 // objList.Add(currToken.tokenData);
//                 // myList.Add((T)currToken.tokenData);
//                 myList.Add(currToken.tokenData);
//                 i += 1;
//             }
//             throw new ArgumentException($"Expected an array close bracket ']'");

//         }

//         public static ParseData<T> ParseObject<T>(List<Token> tokenList, int startPos) where T : class, new(){
//             T typeInst = new T();
//             PropertyInfo[] props = typeof(T).GetProperties();
//             for(int i = startPos; i < tokenList.Count; i++){
//                 Token currToken = tokenList[i + 1];
//                 if(currToken.tokenData.ToString() == "}"){
//                     return new ParseData<T>(i, typeInst);
//                 }else{
//                     if(currToken.tokenType != TokenType.STRING){
//                         throw new ArgumentException($"Expected string key got: {currToken.tokenType}");
//                     }

//                     if(tokenList[i + 2].tokenData.ToString() != ":"){
//                         throw new ArgumentException($"Expected colon after key in object got: {tokenList[i + 2].tokenData}");
//                     }

//                     // Figure out what 'type' is expected for this key
//                     for(int z = 0; z < props.Length; z++){
//                         if(props[z].Name == tokenList[i + 1].tokenData.ToString()){
//                             Type propType = props[z].PropertyType;
//                             if(propType == typeof(string)){
//                                 props[z].SetValue(typeInst, tokenList[i+3].tokenData.ToString());
//                             }else if(propType == typeof(int)){
//                                 props[z].SetValue(typeInst, (int)tokenList[i+3].tokenData);
//                             }
//                             else if(propType == typeof(bool)){
//                                 props[z].SetValue(typeInst, (bool)tokenList[i+3].tokenData);
//                             }else{
//                                 MethodInfo method = typeof(JsonParser).GetMethod(nameof(JsonParser.ParseRecurse), BindingFlags.Static | BindingFlags.Public);
//                                 method = method.MakeGenericMethod(propType);
//                                 // parsedType = method.Invoke(null, new object[]{tokenList, i + 3});
//                                 object parsedType = method.Invoke(null, new object[]{tokenList, i + 3});
//                                 Console.WriteLine();
//                                 // props[z].SetValue(typeInst, Convert.ChangeType(parsedType, propType));
//                                 foreach(PropertyInfo prop in parsedType.GetType().GetProperties()){
//                                     if(prop.Name == "tokensRead"){
//                                         int lastTokenIndex = Convert.ToInt32(prop.GetValue(parsedType)); 
//                                         i = lastTokenIndex + 1;
//                                         // i += Convert.ToInt32(prop.GetValue(parsedType)) - i;
//                                     }else if(prop.Name == "data"){
//                                         props[z].SetValue(typeInst, Convert.ChangeType(prop.GetValue(parsedType), propType));
//                                     }
//                                 }
//                             }
//                         }
//                     }

//                     if(tokenList[i + 4].tokenData.ToString() == "}"){
//                         return new ParseData<T>(i + 4, typeInst);
//                     }else if(tokenList[i + 4].tokenData.ToString() != ","){
//                         throw new ArgumentException($"Expected comma after pair in object got: {tokenList[i+4].tokenData.ToString()}");
//                     }
//                     i += 3;
//                 }

//             }

//             throw new ArgumentException("Expected an end object brace");
//         }
//     }
// }