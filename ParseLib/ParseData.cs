namespace ParseLib
{
    public class ParseData 
    {
        public object data {get; set;}
        public int tokensRead {get;set;}

        public ParseData(int tokensRead, object data){
            this.tokensRead = tokensRead;
            this.data = data;
        }
    }
}