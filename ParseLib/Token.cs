namespace ParseLib
{
    public class Token
    {
        public object tokenData {get; set;}
        public TokenType tokenType {get;set;}
        public int endPosition {get; set;}
        public Token(object data, TokenType tokenType, int endPosition){
            this.tokenData = data;
            this.tokenType = tokenType;
            this.endPosition = endPosition;
        }
    }

    public enum TokenType{
        STRING = 0,
        NUMBER = 1,
        BOOLEAN = 2,
        NULL = 3,
        SYNTAX = 4,
        FLOAT = 5 
    }
}