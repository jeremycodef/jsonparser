using System;
using Xunit;
using ParseLib;
using System.Collections.Generic;

namespace ParseTests
{
    public class JsonLexerTest
    {
        
        [Fact]
        public void TestLexParseBasic()
        {
            List<Token> tokenList = JsonLexer.Lex("{ \"Something\" : \"Something\"}".ToCharArray());
            Assert.Equal(5, tokenList.Count);
        }

        [Fact]
        public void TestLexIntBasic()
        {
            List<Token> tokenList = JsonLexer.Lex("{ \"Something\" : 0 }".ToCharArray());
            Assert.Equal(5, tokenList.Count);
            Assert.Equal(0, tokenList[3].tokenData);
        }

        [Fact]
        public void TestLexBoolBasic()
        {
            List<Token> tokenList = JsonLexer.Lex("{ \"Something\" : true }".ToCharArray());
            Assert.Equal(5, tokenList.Count);
            Assert.Equal(true, tokenList[3].tokenData);
        }

        [Fact]
        public void TestLexNullBasic()
        {
            List<Token> tokenList = JsonLexer.Lex("{ \"Something\" : null }".ToCharArray());
            Assert.Equal(5, tokenList.Count);
            Assert.Equal(null, tokenList[3].tokenData);
        }

        [Fact]
        public void TestAllTogetherNow()
        {
            List<Token> tokenList = JsonLexer.Lex("{ \"String\" : \"String\", \"Number\": 0, \"Bool\": true, \"Null\": null}".ToCharArray());
            Assert.Equal(16, tokenList.Count);
            Assert.Equal("String", tokenList[3].tokenData);
            Assert.Equal(0, tokenList[7].tokenData);
            Assert.Equal(true, tokenList[11].tokenData);
            Assert.Equal(null, tokenList[15].tokenData);
        }

    }
}