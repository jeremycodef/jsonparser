using Xunit;
using ParseLib;
using System.Collections.Generic;
using System;

namespace ParseTests
{
    public class Client{
        public string ClientNumber {get; set;}
        public string EngagementNumber {get; set;}
        public Engagement Eng {get; set;}
        public bool IsActive {get;set;}
        public int Id {get; set;}
        public List<int> EngIdList {get; set;}
    }

    public class Engagement {
        public string EngagementNumber {get; set;}
    }

    public class JsonParserTest
    {
        [Fact]
        public void TestParseBasicObject()
        {
            List<Token> tokenList = JsonLexer.Lex("{ \"ClientNumber\" : \"11199191\"}".ToCharArray());
            Client myClient = NewJsonParser.Parse<Client>(tokenList, 0);
            Assert.Equal("11199191", myClient.ClientNumber);
        }

        [Fact]
        public void TestParseObjectMultipleFields()
        {
            List<Token> tokenList = JsonLexer.Lex("{ \"ClientNumber\" : \"11199191\", \"EngagementNumber\" : \"3333\"}".ToCharArray());
            Client myClient = NewJsonParser.Parse<Client>(tokenList, 0);
            Assert.Equal("11199191", myClient.ClientNumber);
            Assert.Equal("3333", myClient.EngagementNumber);
        }
        [Fact]
        public void TestParseObjectMultipleFieldsWithBool()
        {
            List<Token> tokenList = JsonLexer.Lex("{ \"ClientNumber\" : \"11199191\", \"EngagementNumber\" : \"3333\", \"IsActive\" : true}".ToCharArray());
            Client myClient = NewJsonParser.Parse<Client>(tokenList, 0);
            Assert.Equal("11199191", myClient.ClientNumber);
            Assert.Equal("3333", myClient.EngagementNumber);
            Assert.Equal(true, myClient.IsActive);
        }

        [Fact]
        public void TestParseObjectMultipleFieldsWithBoolAndInt()
        {
            List<Token> tokenList = JsonLexer.Lex("{ \"ClientNumber\" : \"11199191\", \"EngagementNumber\" : \"3333\", \"IsActive\" : true, \"Id\" : 3}".ToCharArray());
            Client myClient = NewJsonParser.Parse<Client>(tokenList, 0);
            Assert.Equal("11199191", myClient.ClientNumber);
            Assert.Equal("3333", myClient.EngagementNumber);
            Assert.Equal(true, myClient.IsActive);
            Assert.Equal(3, myClient.Id);
        }

        [Fact]
        public void TestParseObjectWithArray()
        {
            List<Token> tokenList = JsonLexer.Lex("{ \"EngIdList\" : [1, 2, 3]}".ToCharArray());
            Client myClient = NewJsonParser.Parse<Client>(tokenList, 0);
            // Assert.Equal("11199191", myClient.ClientNumber);
            Assert.Equal(new int[]{1, 2, 3}, myClient.EngIdList.ToArray());
        }

        [Fact]
        public void TestParseSimpleNestedObject()
        {
            List<Token> tokenList = JsonLexer.Lex("{ \"Eng\" : {\"EngagementNumber\" :\"333\"}}".ToCharArray());
            Client myClient = NewJsonParser.Parse<Client>(tokenList, 0);
            Assert.Equal("333", myClient.Eng.EngagementNumber);
        }

        [Fact]
        public void TestParseNestedObject()
        {
            List<Token> tokenList = JsonLexer.Lex("{ \"ClientNumber\" : \"11199191\", \"EngagementNumber\" : \"3333\", \"Eng\" : {\"EngagementNumber\" :\"333\"}, \"IsActive\" : true, \"Id\" : 3}".ToCharArray());
            Client myClient = NewJsonParser.Parse<Client>(tokenList, 0);
            Assert.Equal("11199191", myClient.ClientNumber);
            Assert.Equal("3333", myClient.EngagementNumber);
            Assert.Equal(true, myClient.IsActive);
            Assert.Equal(3, myClient.Id);
        }


        [Fact]
        public void TestParseIntArray()
        {
            List<Token> tokenList = JsonLexer.Lex("[ 3, 2, 1 ]".ToCharArray());
            List<int> myClient = NewJsonParser.Parse<List<int>>(tokenList, 0);
            Assert.Equal(myClient[0], 3);
            Assert.Equal(myClient[1], 2);
            Assert.Equal(myClient[2], 1);
        }

        [Fact]
        public void TestParseBoolArray()
        {
            List<Token> tokenList = JsonLexer.Lex("[ true, false, true ]".ToCharArray());
            List<bool> myClient = NewJsonParser.Parse<List<bool>>(tokenList, 0);
            Assert.Equal(true, myClient[0]);
            Assert.Equal(false, myClient[1]);
            Assert.Equal(true, myClient[2]);
        }
        [Fact]
        public void TestParseStrArray()
        {
            List<Token> tokenList = JsonLexer.Lex("[ \"Something\", \"Else\", \"Here\"]".ToCharArray());
            List<string> myClient = NewJsonParser.Parse<List<string>>(tokenList, 0);
            Assert.Equal("Something", myClient[0]);
            Assert.Equal("Else", myClient[1]);
            Assert.Equal("Here", myClient[2]);
        }

        [Fact]
        public void TestParseObjectArray()
        {
            List<Token> tokenList = JsonLexer.Lex("[ {\"ClientNumber\" : \"1\"}, {\"ClientNumber\" : \"2\"}, {\"ClientNumber\" : \"3\"}]".ToCharArray());
            List<Client> myClient = NewJsonParser.Parse<List<Client>>(tokenList, 0);
            Assert.Equal("1", myClient[0].ClientNumber);
            Assert.Equal("2", myClient[1].ClientNumber);
            Assert.Equal("3", myClient[2].ClientNumber);

        }

        // [Fact]
        // public void TestParseArrayMethod()
        // {
        //     List<Token> tokenList = JsonLexer.Lex("[ 3, 2, 1 ]".ToCharArray());
        //     List<int> myClient = NewJsonParser.ParseArray<List<int>>(tokenList, 0).data;
        //     Assert.Equal(myClient[0], 3);
        //     Assert.Equal(myClient[1], 2);
        //     Assert.Equal(myClient[2], 1);
        // }

        [Fact]
        public void TestParseBaseIntType()
        {
            List<Token> tokenList = JsonLexer.Lex("2".ToCharArray());
            int num = NewJsonParser.Parse<int>(tokenList, 0);
            Assert.Equal(2, num);
        }

        [Fact]
        public void TestParseBaseStringType()
        {
            List<Token> tokenList = JsonLexer.Lex("\"Something\"".ToCharArray());
            string testStr = NewJsonParser.Parse<string>(tokenList, 0);
            Assert.Equal("Something", testStr);
        }

        [Fact]
        public void TestParseNestedIntArray()
        {
            List<Token> tokenList = JsonLexer.Lex("[[1, 2]]".ToCharArray());
            List<List<int>> testIntList = NewJsonParser.Parse<List<List<int>>>(tokenList, 0);
            Console.WriteLine();
            // int[] targetData = new int[2]{1, 2};
            // int[][] intArr = new int[][]{targetData};
            // int[,] resArr =  new int[,]{{}};
            // resArr[0, 0] = testIntList[0].ToArray();
            // Assert.Equal(arr, testIntList.ToArray());
        }

    }
}